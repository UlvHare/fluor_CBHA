---
title: "Создание модели"
date: "`r Sys.Date()`"
editor_options:
  chunk_output_type: console
bibliography: book.bib
csl: maik.csl
---

<!---
Чтобы oбновить документ, запустить в консоли wflow_build("analysis/03_experiment2018a.Rmd")
Чтобы
--->

```{r settings, include = FALSE}
# Global options
# knitr::opts_chunk$set(dev = "png",
#                       dev.args = list(type = "cairo"),
#                       dpi = 100,
#                       fig.align = "center")

knitr::opts_chunk$set(dev = "svglite",
                      dpi = 100,
                      fig.align = "center")

# Ширина строки вывода R в знаках
options(width = 120)

# "Ленивая" подгрузка нужных библиотек, чтобы не подключать каждый раз при компиляции:

required_pkgs <- c("tibble",
                   "kableExtra",
                   "dplyr",
                   "readr",
                   "tidyr",
                   "stringr",
                   "ggplot2",
                   "ggthemes",
                   "pander",
                   "cowplot",
                   "extrafont")

lazy_load <- function(pkgs) {
  attached <- search()
  attached_pkgs <- attached[grepl("package", attached)]
  need_to_attach <- pkgs[which(!pkgs %in% gsub("package:", "", attached_pkgs))]
  if (length(need_to_attach) == 0) {
  message("\n ...Нужные библиотеки уже подключены!\n")
  } else {
    for (i in 1:length(need_to_attach)) {
      require(need_to_attach[i], character.only = TRUE)
    }
  }
}

lazy_load(required_pkgs)

# Максимальная ширина таблицы pander в знаках
panderOptions("table.split.table", 200)

# Тема ggplot
theme_set(theme_linedraw(base_size = 16, base_family = "Georgia")) +
          theme_update(plot.background = element_rect(fill = "#bfc4bb",
                                                        colour = "#bfc4bb"),
                       rect = element_rect(fill = "#bfc4bb"),
                       legend.key = element_rect(fill = "#bfc4bb"),
                       panel.background = element_rect(fill = "#bfc4bb"))
```

# Формулировка модели

Вспоминаем формулировку задачи:

> Влияет ли концентрация фермента в реакционной кювете на результат измерения ЦА и, если да, то меняется ли
это влияние при разных способ экстракции фермента из химуса?

В модели *целлобиогидролазной активности* (ЦА) как функции от ковариат я предполагаю, что ЦА распределена
нормально (см. квантильный график в предыдущем разделе). Фиксированными переменными являются *концентрация химуса* (интервальная) и *экстрагент* (фактор с двумя уровнями). Возможны взаимодействия между фиксированными эффектами, т.е., *экстрагент* × *концентрация химуса*. Чтобы учесть зависимость измерений из одной пробирки вводим *пробирки* как случайный эффект.

Следует учесть и вложенность факторов: *экстрагент* &rarr; *пробирки* &rarr; *концентрация химуса*. Обычно в
линейных моделях вложенные *фиксированные* эффекты рассматривают просто как взаимодействующие.

Формула "максимальной" модели (все эффекты и их взаимодействия) будет такой:

$$
\begin{equation}
\begin{split}
cbha_{ij} & = \alpha + \beta_1 \times extragent_{ij} + \beta_2 \times conc_{ij} + \\
& + \beta_3 \times extragent_{ij} \times conc_{ij} + sample_{i} + \varepsilon_{ij}
\end{split}
\end{equation}
$$

Где:

$cbha_{ij}$ --- ЦА *j*-го измерения из *i*-й пробирки; *i* &isin; [1,20]; *j* &isin; [1,3]

$\alpha$ --- константа (оно же intercept)

$\beta_n$ --- угловые коэффициенты (slopes)

$extragent_{ij}$ --- экстрагент

$conc_{ij}$ --- концентрация химуса

$sample_{i}$ --- пробирка (случайный эффект)

$\varepsilon_{ij}$ --- случайная ошибка (остатки модели, "шум")

$cbha_{ij} \sim N(0, \sigma^2)$

$sample_{i} \sim N(0, \sigma_{sample}^2)$

$\varepsilon_{ij} \sim N(0, \sigma^2)$

По графикам заметно, что ЦА растёт пропорционально концентрации фермента (мы на линейной части кинетической
кривой), и происходит это в глицерине и в буфере, возможно, по-разному. Кроме того, медианы ЦА в глицерине и
буфере не сильно различаются. Так что, подтверждается выбранная структура фиксированной части `~ conc *
extragent` с особым интересом к взаимодействию этих эффектов (`conc : extragent`). По пробиркам ЦА скачет,
так что, случайную константу (`1 | sample`) включаем однозначно, а вот со случайными углами всё несколько
сложнее. По смыслу и графикам угол меняется и по экстрагенту, и по концентрации, так что с учётом того, что в
каждой пробирке только 1 экстрагент, случайная часть должна быть `(1 | extragent / sample / conc)`. Однако,
такая навороченная структура на маленькой выборке почти наверняка не сойдётся. Вообще, для оценки дисперсии в
случайной части крайне желательно минимум 5 уровней фактора, а у нас всего 2 экстрагента и 3 концентрации, так
что вряд ли удастся их загнать в случайную часть. Скорее всего, дисперсия по углам наклона войдёт в "шум".

```{block, type = "NB"}
Погонял разные модели на `nlme` --- результаты сомнительные. Повышение концентрации химуса (а,
следовательно, и фермента) повышает среднюю ЦА, а буфер в качестве экстрагента --- понижает. Ну это и
предполагалось, так что хорошо. Взаимодействие эффектов вроде как незначимо. А по смыслу --- вполне себе: в
разных экстрагентах повышение концентрации по-разному увеличивает ЦА.
```

# Создание и подгонка модели

Здесь я пойду по пути, описанному в гл. 5 книги Жюра и соавторов про смешанные модели в экологии
[@ZuurIWSS2009]. Используется нисходящая стратегия: сначала строится "запредельная" модель со всеми возможными
эффектами и их взаимодействиями, затем она по мере возможности упрощается и проверяется.

1. Начинаем с самой полной модели, содержащей все фиксированные эффекты и все их возможные взаимодействия.
Если это не подходит, например, слишком много независисимых переменных, выбираем из них самые важные по
смыслу.

2. На "запредельной" модели подбираем оптимальную структуру случайного компонента. Для сравнения моделей с
одинаковой фиксированной частью используется REML, методы --- LRT, AIC, BIC. Уровни значимости будут завышены.

3. Подобрав оптимальную структуру случайной части, переходим к фиксированной. Для выяснения оптимальной
структуры можно использовать статистики *F* или *t*, полученные методом REML, или же сравнивать вложенные
модели, т.е., исключать фиксированные предикторы по одному (а сначала --- их взаимодействия). Для сравнения
моделей со вложенными фиксированными эффектами (и одинаковой случайной частью) используется только метод ML,
а никак не REML.

4. Строим итоговую модель методом REML и тестируем её.

## Преобразование исходных данных

Для регрессионных моделей с взаимодействием ковариат рекомендуют эти самые ковариаты стандартизовать (привести
к одной шкале) и даже нормализовать. Однако, интерпретировать такие преобразования не всегда возможно, и смысл
это имеет только для предсказательных моделей (у нас --- в первую очередь описательная). Поэтому сделаем
компромиссный вариант: переведём ЦА в нмоль/мин, а концентрацию --- в г/л, просто умножив на 1000 и то и
другое. Тогда соотношения сохранятся, а считать будет проще (м.б., и модель лучше сойдётся). Заодно и
переменные назовём более понятно.

```{r loadConc, message=F}
rabbitConc <- read_csv2("data/rabbit2018c.csv", col_types = cols(sample = col_character(),
                                                       loc_part = col_factor(),
                                                       type = col_factor()
                                                       )
)

concWork <- rabbitConc %>%
  transmute(sample = factor(sample),
            extragent = type,
            conc = C_chym * 1000,
            cbha2 = cbha * 1000)

str(concWork)
```

Теперь наши переменные:

1. `cbha2` --- целлобиогидролазная активность (ЦА), нмоль/мин. Исследуемая (зависимая) переменная.
2. `conc` --- концентрация химуса в кювете, г/л.
3. `extragent` --- экстрагент (чем экстрагировался фермент).
4. `sample` --- пробирка, из каждой отбирали по 3 повторности разных концентраций.


## Построение максимальной модели. Подборка структуры случайного компонента.

В книге [@ZuurIWSS2009] предлагается также совсем развёрнутый протокол (гл. 4), где в сравнении моделей
участвует нулевая --- без учёта случайных эффектов. Суть в формальной проверке, нужны ли они. Однако, в
данном случае случайный эффект пробирки диктует сама структура эксперимента, да и на графиках видно.

Для построения смешанных моделей в `R` создано довольно много пакетов, наиболее известны среди них `nlme` и
`lme4`. Оба по умолчанию используют метод REML, так что на данном этапе явно его задавать не надо. У обоих
есть свои преимущества и недостатки, но по `lme4` больше документации, так что попробуем начать с него.

### Полная гнездовая структура (3 уровня)

Попробую всё-таки задать модель "как в жизни", т.е., полное гнездование.

```{r fullNest, error=TRUE, message=FALSE}
library(lme4)

Mod1 <- lmer(cbha2 ~ conc * extragent + (1 | extragent / sample / conc), data = concWork)
```

Ошибочка вышла! Болкер пишет, что её можно убрать (`lmerControl(check.nobs.vs.nlev="ignore")`{.R}; see
`?lmerControl` for details), и оно даже будет работать, но такая модель будет делить дисперсии остатков и
взаимодействия `extragent:sample:conc` произвольным образом, так что ну её.

Внезапно нашлось (Bates, 2010), что в `lme4` есть функция проверки гнездования факторов. Представим
концентрацию как фактор с 3 уровнями и посмотрим:

```{r testNest}
with(concWork, isNested(as.factor(conc), sample))
with(concWork, isNested(sample, extragent))
```

Опа! А концентрации-то по пробиркам не гнездятся (формально). Очевидно, потому что во всех пробирках смотрели
одинаковые (неуникальные) концентрации.

### Убираем уровень концентраций

```{r nest1}
Mod2 <- lmer(cbha2 ~ conc * extragent + (1 | extragent / sample), data = concWork)
```

Ругается на сингулярность, отрицательные eigenvalues и пр., причём по-разному на разных прогонах по одним и
тем же данным. Можно издеваться с оптимизаторами, но по сути это означает, что слишком мало данных на такую
сложную структуру.

И ещё гнездовая структура:

```{r nest2}
Mod3 <- lmer(cbha2 ~ conc * extragent + (conc | extragent / sample), data = concWork)
```

Схожая ситуация.

### Пробуем все возможные варианты

Ищем компромисс между смыслом и математикой. Создаём все возможные модели, отказываемся, если не сходятся, и
смотрим диагностические графики.


```{r testAllModels, error=TRUE}
Mod4 <- lmer(cbha2 ~ conc * extragent + (extragent | sample), data = concWork)
Mod5 <- lmer(cbha2 ~ conc * extragent + (conc | sample), data = concWork)
Mod6 <- lmer(cbha2 ~ conc * extragent + (1 | extragent : sample), data = concWork)
Mod7 <- lmer(cbha2 ~ conc * extragent + (1 | conc : sample), data = concWork)
Mod8 <- lmer(cbha2 ~ conc * extragent + (0 + extragent | sample), data = concWork)
Mod9 <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWork)
Mod10 <- lmer(cbha2 ~ conc * extragent + (1 | sample), data = concWork)
```

Итак, совсем сошлось:

1. Mod4: `(extragent | sample)`
2. Mod6: `(1 | extragent : sample)`
3. Mod9: `(0 + conc | sample)`
4. Mod10: `(1 | sample)`

Убираем всё, чтобы не путаться и строим заново по мере упрощения:

```{r}
rm(Mod2, Mod3, Mod4, Mod5, Mod6, Mod8, Mod9, Mod10)

Mod1 <- lmer(cbha2 ~ conc * extragent + (1 | extragent : sample), data = concWork)
Mod2 <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWork)
Mod3 <- lmer(cbha2 ~ conc * extragent + (extragent | sample), data = concWork)
Mod4 <- lmer(cbha2 ~ conc * extragent + (1 | sample), data = concWork)
```

### Сравнение моделей

Итак, у нас сошлись 4 модели. Сравним их по графикам остатков.

```{r diaPlot1, fig.cap="Сравнение моделей. Первичные диагностические графики."}
cowplot::plot_grid(plot(Mod1), plot(Mod2), plot(Mod3), plot(Mod4),
          ncol = 2, labels = c('Mod1', 'Mod2', 'Mod3', 'Mod4'))
```

Паттерны (ковши) намечаются во всех моделях, но картину смазывают экстремумы. М.б., их всё-таки стоит убрать.
Как минимум, исследовать. По результатам, возможно, придётся повторить подгонку структуры случайного
компонента на урезанной выборке.

# Влиятельные точки

Для проверки влиятельных точек в пакете `car` есть удобные функции `infIndexPlot()`{.R} и
`influencePlot()`{.R}, но они требуют, чтобы исходная таблица была простой `dataframe`, а не `tibble`, и
желательно с понятными `rownames`. Пересоздаю модели по такой таблице.

```{r}
library(forcats)
concWorkDf <- concWork %>%
  mutate(extragent = fct_recode(extragent,  Gly = "глицерин", Buf = "буфер"),
         rname = paste0(sample, extragent, conc)) %>%
  column_to_rownames(var = "rname")

Mod1 <- lmer(cbha2 ~ conc * extragent + (1 | extragent : sample), data = concWorkDf)
Mod2 <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWorkDf)
Mod3 <- lmer(cbha2 ~ conc * extragent + (extragent | sample), data = concWorkDf)
Mod4 <- lmer(cbha2 ~ conc * extragent + (1 | sample), data = concWorkDf)
```

Как выглядят влиятельные точки на графике `infIndexPlot()`{.R} (на примере модели №1):

```{r influencePlot1, fig.cap="Отображение влиятельных точек через **infIndexPlot()**"}
library(car)
infIndexPlot(Mod1)
```

...и на графике `influencePlot()`{.R}:


```{r influencePlot2, fig.cap="Отображение влиятельных точек через **influencePlot()**"}
influencePlot(Mod1)
```

Точки те же, но на втором графике видно лучше, так что для проверки всех моделей исаользуем
`influencePlot()`{.R}.

```{r influencePlot3, fig.cap="Влиятельные точки по всем моделям"}
op <- par(family = "Georgia", ps = 16, mfrow = c(2, 2), mar = c(5, 5, 2, 2))
influencePlot(Mod1, main = "Mod1")
influencePlot(Mod2, main = "Mod2")
influencePlot(Mod3, main = "Mod3")
influencePlot(Mod4, main = "Mod4")
par(op)
```

Во всех случаях влияет 25-я пробирка, причём концентрации 5 и 10 г/л, а не экстремальные 10 и 15 из
разведанализа.

Для очистки совести посмотрим ещё диагностические графики для простой линейной модели (без случайных
эффектов).

```{r lmInfluencePlot, fig.cap="Диагностические графики простой линейной модели"}
par(mfrow = c(2, 2))
plot(lm(cbha2 ~ conc * extragent, data = concWorkDf))
```

Поскольку графики из разведанализа тоже прозрачно намекают на 25-ю пробирку, получается, что стоит
попробовать её исключить всю.

```{r}
concWorkClean <- subset(concWorkDf, sample != 25) %>% droplevels()
str(concWorkClean)
```

# Максимальная модель. 2-я попытка.

Строим максимальную модель на урезанной выборке. Полную гнездовую структуру пробовать не буду --- данных стало ещё меньше.

## Пробуем все возможные варианты

Ищем компромисс между смыслом и математикой. Создаём все возможные модели, отказываемся, если не сходятся, и
смотрим диагностические графики.


```{r testAllModels2, error=TRUE}
Mod1 <- lmer(cbha2 ~ conc * extragent + (1 | extragent / sample), data = concWorkClean)
Mod2 <- lmer(cbha2 ~ conc * extragent + (conc | extragent / sample), data = concWorkClean)
Mod3 <- lmer(cbha2 ~ conc * extragent + (extragent | sample), data = concWorkClean)
Mod4 <- lmer(cbha2 ~ conc * extragent + (conc | sample), data = concWorkClean)
Mod5 <- lmer(cbha2 ~ conc * extragent + (1 | extragent : sample), data = concWorkClean)
Mod6 <- lmer(cbha2 ~ conc * extragent + (1 | conc : sample), data = concWorkClean)
Mod7 <- lmer(cbha2 ~ conc * extragent + (0 + extragent | sample), data = concWorkClean)
Mod8 <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWorkClean)
Mod9 <- lmer(cbha2 ~ conc * extragent + (1 | sample), data = concWorkClean)
```

Итак, совсем сошлось:

1. Mod3: `(extragent | sample)`
2. Mod4: `(conc | sample)`
3. Mod5: `(1 | extragent : sample)`
4. Mod8: `(0 + conc | sample)`
5. Mod9: `(1 | sample)`

Убираем всё, чтобы не путаться и строим заново по мере упрощения:

```{r}
rm(Mod1, Mod2, Mod3, Mod4, Mod5, Mod7, Mod8, Mod9)

Mod1 <- lmer(cbha2 ~ conc * extragent + (1 | extragent : sample), data = concWorkClean)
Mod2 <- lmer(cbha2 ~ conc * extragent + (extragent | sample), data = concWorkClean)
Mod3 <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWorkClean)
Mod4 <- lmer(cbha2 ~ conc * extragent + (conc | sample), data = concWorkClean)
Mod5 <- lmer(cbha2 ~ conc * extragent + (1 | sample), data = concWorkClean)
```

Итак, у нас сошлись 5 моделей. Сравним их по графикам остатков, AICc и как ложатся предсказания на
реальные данные.

## Остатки ~ предсказанное по всем моделям

```{r diaPlot2, fig.width = 11, fig.cap="Сравнение моделей v2. Первичные диагностические графики."}
cowplot::plot_grid(plot(Mod1), plot(Mod2), plot(Mod3), plot(Mod4), plot(Mod5),
          ncol = 3, labels = c('Mod1', 'Mod2', 'Mod3', 'Mod4', 'Mod5'))
```

В целом смотрится лучше, чем на рис. \@ref(fig:diaPlot1) --- на 3-й и 4-й паттерны не просматриваются, да и на
остальных не так явно.

Посмотрим поподробнее на модели 3 и 4.

```{r moddiag3, fig.cap="Модель №3. Диагностические графики"}
lmerResPlots <- function(model) {
  # DF с остатками
  # broom.mixed всё ещё "under construction", сейчас лучше fortify.merMod()
  modData <- fortify.merMod(model)
  # Список имён эффектов
  effNames <- all.vars(delete.response(terms(model)))

  effPlots <- purrr::map(effNames,
    ~ggplot(modData, aes(x = .data[[.x]], y = .scresid)) + {
    if(is.factor(modData[[.x]]))
      geom_boxplot()
    else
      geom_point(size = 2, alpha = 0.4, color = "darkgreen")
    } +
    xlab(.x)
  ) %>% cowplot::plot_grid(plotlist = .)

  cowplot::plot_grid(ggplot(modData, aes(.fitted, .scresid)) +
                   geom_hline(yintercept = 0) +
                   geom_point(),
            effPlots, ncol = 1)
}

lmerResPlots(Mod3)
```


```{r moddiag4, fig.cap="Модель №4. Диагностические графики"}
lmerResPlots(Mod4)
```

Модель №3 поаккуратнее в плане дисперсий остатков по эффектам.

## Критерий Акаике

```{r modInfSel}
library(MuMIn)
model.sel(Mod1, Mod2, Mod3, Mod4, Mod5)
```

Опять-таки 3-я и 4-я модели.

## Предсказания моделей на реальные данные

Рисуем предсказания моделей по каждой пробирке относительно реальных данных.

(ref:spPlots) Спагетти с горошком по всем моделям. Отсортировано по экстрагенту и средней ЦА.

```{r spPlots, fig.width = 11, fig.cap = "(ref:spPlots)"}
concWorkClean %>%
  mutate(Mod1 = fitted(Mod1),
         Mod2 = fitted(Mod2),
         Mod3 = fitted(Mod3),
         Mod4 = fitted(Mod4),
         Mod5 = fitted(Mod5)) %>%
  ggplot(aes(x = conc, y = cbha2)) +
  geom_line(aes(y = Mod1, colour = "Mod1")) +
  geom_line(aes(y = Mod2, colour = "Mod2")) +
  geom_line(aes(y = Mod3, colour = "Mod3")) +
  geom_line(aes(y = Mod4, colour = "Mod4")) +
  geom_line(aes(y = Mod5, colour = "Mod5")) +
  geom_point(size = 2, shape = 1) +
  scale_x_continuous(breaks = c(5,10,15)) +
  facet_wrap(~ extragent:reorder(sample, cbha2), nrow = 2) +
  theme(legend.position = "bottom") +
  scale_colour_brewer(palette = "Set1")
```

Не там перенёс графики, но в целом понятно, что вновь лидируют 3-я и 4-я модели, в буфере идеальна 4-я.

## Предсказания на уровне выборки

Зная, что почти не отличаются, смотрю только лидеров. Задаю концентрации как фактор, чтобы посмотреть усатые
ящики.

```{r popLevel, fig.width=11, fig.cap = "Предсказания моделей на уровне выборки"}
concWorkClean %>%
  mutate(concF = as.factor(conc),
         Mod3 = predict(Mod3, re.form = NA),
         Mod4 = predict(Mod4, re.form = NA)) %>%
ggplot(aes(x = concF, y = cbha2)) +
  geom_boxplot() +
  geom_line(aes(x = as.numeric(concF), y = Mod3, colour = "Mod3"), size = 1) +
  geom_line(aes(x = as.numeric(concF), y = Mod4, colour = "Mod4"), size = 1, linetype = 2) +
  facet_wrap(~extragent) +
  scale_colour_calc()
```

3-я и 4-я вообще сливаются, пришлось исхитриться и сделать прерывистую линию, чтобы увидеть.

## ANOVA

```{r}
anova(Mod3, Mod4)
```

Различия незначительные во всех смыслах.

## Выводы

По совокупности оптимальны 2 максимальные модели:

- **№3** --- `cbha2 ~ conc * extragent + (0 + conc | sample)`{.R}
- **№4** --- `cbha2 ~ conc * extragent + (conc | sample)`{.R}

Суть такой структуры случайных эффектов:

- Случайный угол наклона зависимости ЦА от концентрации, различный в разных пробах.

  + **№3** --- константа (среднее значение ЦА при концентрации химуса 5 г/л) по пробиркам не различается,
т.е., этот разброс случаен
  + **№4** --- константа зависит от пробы.

Эти модели слабо различаются между собой, №3 чуть-чуть получше по информационным критериям и по графикам
остатков в зависимости от эффектов. Подиагностируем обе.

```{r cleanModels}
rm(Mod1, Mod2, Mod3, Mod4, Mod5)
modMax <- lmer(cbha2 ~ conc * extragent + (0 + conc | sample), data = concWorkClean)
```

Максимальная модель создана, теперь её надо подиагностировать и по возможности сократить.




# Литература
