---
title: "Создание финальной модели"
date: "`r Sys.Date()`"
editor_options:
  chunk_output_type: console
bibliography: book.bib
csl: maik.csl
---

<!---
Чтобы oбновить документ, запустить в консоли wflow_build("analysis/03_experiment2018a.Rmd")
Чтобы
--->

```{r settings, include = FALSE}
# Global options
# knitr::opts_chunk$set(dev = "png",
#                       dev.args = list(type = "cairo"),
#                       dpi = 100,
#                       fig.align = "center")

knitr::opts_chunk$set(dev = "svglite",
                      dpi = 100,
                      fig.align = "center",
                      autodep = TRUE) # Для корректного кэширования

# Ширина строки вывода R в знаках
options(width = 120)

# "Ленивая" подгрузка нужных библиотек, чтобы не подключать каждый раз при компиляции:

required_pkgs <- c("tibble",
                   "kableExtra",
                   "dplyr",
                   "readr",
                   "tidyr",
                   "stringr",
                   "ggplot2",
                   "ggthemes",
                   "pander",
                   "cowplot",
                   "extrafont")

lazy_load <- function(pkgs) {
  attached <- search()
  attached_pkgs <- attached[grepl("package", attached)]
  need_to_attach <- pkgs[which(!pkgs %in% gsub("package:", "", attached_pkgs))]
  if (length(need_to_attach) == 0) {
  message("\n ...Нужные библиотеки уже подключены!\n")
  } else {
    for (i in 1:length(need_to_attach)) {
      require(need_to_attach[i], character.only = TRUE)
    }
  }
}

lazy_load(required_pkgs)

# Максимальная ширина таблицы pander в знаках
panderOptions("table.split.table", 200)

# Тема ggplot
theme_set(theme_linedraw(base_size = 16, base_family = "Georgia")) +
          theme_update(plot.background = element_rect(fill = "#bfc4bb",
                                                        colour = "#bfc4bb"),
                       rect = element_rect(fill = "#bfc4bb"),
                       legend.key = element_rect(fill = "#bfc4bb"),
                       panel.background = element_rect(fill = "#bfc4bb"))
```

# Оптимизация фиксированной части модели

## Тесты полной модели

### Штатные методы `lme4`

```{r, message=F}
rabbitConc <- read_csv2("data/rabbit2018c.csv", col_types = cols(sample = col_character(),
                                                       loc_part = col_factor(),
                                                       type = col_factor()
                                                       )
)

library(forcats)
concWorkClean <- rabbitConc %>%
  transmute(sample = factor(sample),
            extragent = type,
            conc = C_chym * 1000,
            cbha2 = cbha * 1000) %>%
  mutate(extragent = fct_recode(extragent,  Gly = "глицерин", Buf = "буфер"),
         rname = paste0(sample, extragent, conc)) %>%
  column_to_rownames(var = "rname") %>%
  subset(sample != 25) %>%
  droplevels()


library(lme4)

modMax <- lmer(cbha2 ~ conc * extragent + (conc | sample), data = concWorkClean)


drop1(modMax,test="Chisq")

modMaxML <- update(modMax, REML = FALSE)
modRedML <- update(modMaxML,. ~ . - conc:extragent)
anova(modMaxML, modRedML)
```

На грани: хи-квадрат --- ни то ни сё, BIC уменьшается, AIC растёт

### Сравнение по AIC(c)

```{r mumin}
library(MuMIn)
options(na.action = "na.fail")
dredge(modMaxML)
```

Здесь тоже чуточку лучше полная модель.

### Кенвард-Роджер

Здесь надо REML.

```{r KR}
library(pbkrtest)
modRed <- update(modMax,. ~ . - conc:extragent)

KRmodcomp(modMax, modRed)
```

Со скрипом, но можно исключить взаимодействие.

### Бутстреп

```{r}
# Библиотеки в кэш не грузят
library(parallel)
# Number of cores:
(nc <- detectCores())
```


```{r bootstr, cache = TRUE}
# Create clusters
cl <- makeCluster(rep("localhost", nc))
PBmodcomp(modMaxML, modRedML, nsim = 1000, cl = cl)
stopCluster(cl)
rm(cl)
```

Взаимодействие исключить можно.

```{block, type ="NB"}
Удобочитаемый выхлоп по всем методам (LRT, Кенвард-Роджер и бутстреп) даёт `afex`. Он выдаёт и некошерные для
`lmer()`{.R} *p*-значения и степени свободы, которые из-за ненадёжности не совсем прилично включать в статью,
но для прикидки вполне себе удобно.
```

## Сравнение по таблице и графикам

Наконец, полную и сокращённую модели можно сравнить чисто визуально, по объединённой суммарной таблице и
совместным диагностическим графикам. Пакеты `sjPlot`, `performance`, `MuMIn` и `jtools` выдают в таблице
$R^2$ [@NakagS2013;@Johns2014]. Но `sjPlot` --- жуткий комбайн, к тому же перешибает `plot_grid`, так что
постараюсь обойтись без него. Для графиков, возможно, пригодится `ggeffects`.

### Таблица полной и сокращённой моделей

```{r modTable}
library(jtools)
export_summs(modMax, modRed, digits = 3,
             model.names = c("modMax", "modRed"),
             error_format = "CI [{conf.low}, {conf.high}]")
```

Интересно, что в сокращённой модели влияние экстрагента недостоверно.  Коэффициент детерминации $R^2$: в
смешанных (многоуровневых) моделях делится на два варианта --- предельный и условный. Предельный
$R^2_{marginal}$ показывает долю дисперсии, объяснённую только фиксированными эффектами, без учёта случайных.
Условный $R^2_{conditional}$ показывает долю дисперсии, объяснённую как фиксированными, так и случайными
эффектами.

Так вот, в полной модели предельный $R^2$ больше почти на 2%, т.е., больше дисперсии объясняется влиянием
концентрации и экстрагента, а условный меньше лишь на 0.2%, т.е. общее качество модели почти не отличается по
этому критерию. Аргумент против сокращения.

### График фиксированных эффектов

```{r compare1, fig.cap="Сравнение фиксированных эффектов"}
plot_coefs(modMax, modRed, model.names = c("modMax", "modRed"),
                           colors = "Qual1") +
  theme_grey(base_size = 16, base_family = "Georgia")
```

Выводы те же, что из таблицы: при сокращении теряется эффект экстрагента, правда ли это --- смотрим дальше
графики.

### Предсказания моделей на уровне выборки

```{r, fig.width=11, fig.cap = "Предсказания моделей на уровне выборки"}
concWorkClean %>%
  mutate(concF = as.factor(conc),
         Full = predict(modMax, re.form = NA),
         Red = predict(modRed, re.form = NA)) %>%
ggplot(aes(x = concF, y = cbha2)) +
  geom_boxplot() +
  geom_line(aes(x = as.numeric(concF), y = Full, colour = "Full"), size = 1) +
  geom_line(aes(x = as.numeric(concF), y = Red, colour = "Red"), size = 1) +
  facet_wrap(~extragent) +
  scale_colour_calc()
```

Максимальная модель ложится лучше.

### Доверительные интервалы с учётом случайных эффектов (по пробиркам)

(ref:CI) Доверительные интервалы по пробиркам. Зелёный --- полная модель, красный --- сокращённая

```{r}
# Библиотеки в кэш не грузят
library(ciTools)
```

```{r CI, fig.width=11, fig.cap="(ref:CI)"}
concWorkClean %>%
  add_ci(modMax, includeRanef = T, type = "boot", yhatName = "predF", names = c("LF", "UF"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  add_ci(modRed, includeRanef = T, type = "boot", yhatName = "predR",  names = c("LR", "UR"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  ggplot(aes(x = conc)) +
    geom_point(aes(y = cbha2, colour = extragent)) +
    geom_ribbon(aes(ymin = LR, ymax = UR), colour = "red1", fill = "red1", alpha = 0.2, lty = 2) +
    geom_line(aes(y = predR), colour = "red1") +
    geom_ribbon(aes(ymin = LF, ymax = UF), colour="forestgreen", fill="forestgreen", alpha = 0.2, lty=2) +
    geom_line(aes(y = predF), colour = "forestgreen") +
    facet_wrap(~ sample, nrow = 2) +
    theme(
      legend.position = c(1, 0),
      legend.justification = c("right", "bottom")
    )
```

Разница очень мала.

### Доверительные интервалы на уровне выборки

```{r CI2, fig.width=11, fig.cap="Доверительные интервалы на уровне выборки"}
concWorkClean %>%
  add_ci(modMax, includeRanef = F, type = "boot", yhatName = "predF", names = c("LF", "UF"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  add_ci(modRed, includeRanef = F, type = "boot", yhatName = "predR",  names = c("LR", "UR"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  ggplot(aes(x = conc)) +
    geom_point(aes(y = cbha2)) +
    geom_ribbon(aes(ymin = LR, ymax = UR), colour = "red1", fill = "red1", alpha = 0.2, lty = 2) +
    geom_line(aes(y = predR), colour = "red1") +
    geom_ribbon(aes(ymin = LF, ymax = UF), colour="forestgreen", fill="forestgreen", alpha = 0.2, lty=2) +
    geom_line(aes(y = predF), colour = "forestgreen") +
    facet_wrap(~ extragent)
```

По крайней мере в глицерине сокращённая ложится лучше.

## Результат

Всё-таки попробую сократить модель.

# Сокращённая модель

## Тесты сокращённой модели

```{r}
library(performance)
```

```{r}
drop1(modRed, test = "Chisq")
modRed2 <- update(modRed, .~. -extragent)
anova(modRed, modRed2)

KRmodcomp(modRed, modRed2)

# Create clusters
cl <- makeCluster(rep("localhost", nc))
PBmodcomp(modRed, modRed2, nsim = 1000, cl = cl)
stopCluster(cl)
rm(cl)

compare_performance(modMax, modRed, modRed2) %>% plot()
```

На этом графике, чем шире простирается модель по всем параметрам, тем лучше. Выходит, что минимальная лучше всех.

## Сравнение по таблице и графикам

### Таблица сокращённых моделей

```{r modTable2}
export_summs(modRed, modRed2,
             model.names = c("modRed", "modMin"),
             digits = 3, error_format = "CI [{conf.low}, {conf.high}]")
```

В минимальной модели предельный $R^2$ немного больше, т.е., больше дисперсии объясняется влиянием концентрации, а условный меньше лишь на 0.3%, т.е. общее качество модели почти не отличается по этому критерию. Аргумент за сокращение.

### График фиксированных эффектов

```{r compare2, fig.cap="Сравнение фиксированных эффектов"}
plot_coefs(modRed, modRed2, model.names = c("modRed", "modRed2"),
                           colors = "Qual1")
```

"Сила" эффекта концентрации одинакова.

### Предсказания моделей на уровне выборки
```{r, fig.width=11, fig.cap = "Предсказания моделей на уровне выборки"}
concWorkClean %>%
  mutate(concF = as.factor(conc),
         Red2 = predict(modRed2, re.form = NA),
         Red = predict(modRed, re.form = NA)) %>%
ggplot(aes(x = concF, y = cbha2)) +
  geom_boxplot() +
  geom_line(aes(x = as.numeric(concF), y = Red2, colour = "Red2"), size = 1) +
  geom_line(aes(x = as.numeric(concF), y = Red, colour = "Red"), size = 1) +
  facet_wrap(~extragent) +
  scale_colour_calc()
```

В глицерине минимальная модель ложится лучше.

### Доверительные интервалы с учётом случайных эффектов (по пробиркам)

(ref:CI3) Доверительные интервалы по пробиркам. Зелёный --- минимальная модель, красный --- сокращённая

```{r CI3, fig.width=11, fig.cap="(ref:CI3)"}
concWorkClean %>%
  add_ci(modRed2, includeRanef = T, type = "boot", yhatName = "predF", names = c("LF", "UF"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  add_ci(modRed, includeRanef = T, type = "boot", yhatName = "predR",  names = c("LR", "UR"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  ggplot(aes(x = conc)) +
    geom_point(aes(y = cbha2, colour = extragent)) +
    geom_ribbon(aes(ymin = LR, ymax = UR), colour = "red1", fill = "red1", alpha = 0.2, lty = 2) +
    geom_line(aes(y = predR), colour = "red1") +
    geom_ribbon(aes(ymin = LF, ymax = UF), colour="forestgreen", fill="forestgreen", alpha = 0.2, lty=2) +
    geom_line(aes(y = predF), colour = "forestgreen") +
    facet_wrap(~ sample, nrow = 2) +
    theme(
      legend.position = c(1, 0),
      legend.justification = c("right", "bottom")
    )
```

Разница очень мала.

### Доверительные интервалы на уровне выборки

```{r CI4, fig.width=11, fig.cap="Доверительные интервалы на уровне выборки"}
concWorkClean %>%
  add_ci(modRed2, includeRanef = F, type = "boot", yhatName = "predF", names = c("LF", "UF"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  add_ci(modRed, includeRanef = F, type = "boot", yhatName = "predR",  names = c("LR", "UR"),
                  nSims = 1000, parallel = "multicore",
                  ncpus = detectCores()) %>%
  ggplot(aes(x = conc)) +
    geom_point(aes(y = cbha2)) +
#     geom_smooth(aes(y = cbha2)) +
    geom_ribbon(aes(ymin = LR, ymax = UR), colour = "red1", fill = "red1", alpha = 0.2, lty = 2) +
    geom_line(aes(y = predR), colour = "red1") +
    geom_ribbon(aes(ymin = LF, ymax = UF), colour="forestgreen", fill="forestgreen", alpha = 0.2, lty=2) +
    geom_line(aes(y = predF), colour = "forestgreen") +
    facet_wrap(~ extragent)
```

Минимальная модель не хуже сокращённой.


## Выводы

Пробуем минимальную.



# Литература
